#!/bin/bash
echo "Test: crashonerror in Bash"
. ShellUtils
crashonerror
echo "RUN TESTS IN: Bash" >&2
bash TestShellUtils_AllShells.sh
echo "RUN TESTS IN: Zsh" >&2
zsh TestShellUtils_AllShells.sh
