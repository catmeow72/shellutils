#!/usr/bin/env -Ssh -c 'echo This script offers no useful functionality unless sourced. >&2; See README.md for more information. >&2; exit 1'

# Checks if a string can be used as an integer.
isinteger() {
        case "$1" in
                ''|[!-]*[!0-9]*|-*[!0-9]*)
                        return 1
                        ;;
                *)
                        return 0
                        ;;
        esac
}

# Checks if a string is an unsigned integer
# First checks if it is an integer,
# then checks if it has a negative sign
isunsigned() {
	isinteger "$1"
	if [ $? -eq 1 ]; then
		return 1
	fi
	case "$1" in
		-*)
			return 1
			;;
		*)
			return 0
			;;
	esac
}

# Gets a substring of string $1 starting at $2 with length $3
# Exit code 1: First two arguments are not unsigned integers
# Exit code 2: Requested substring out of range
getstringpart() {
        if ! isunsigned "$2"; then
                return 1
        fi
        if ! isunsigned "$3"; then 
            return 1
        fi
        if [ "`mathexpression $3+$2`" -gt "$(printf "%s" "$1" | wc -c)" ]; then
                return 2
        fi
        printf "%s" "$1" | dd bs=1 count=$3 seek=$2 status=none
}

# Gets character $1 in string $2
# If it was impossible to get the characters, puts "?" in stdout and returns a non-success exit code
# Exit code 1: First argument is not an unsigned integer
# Exit code 2: First argument is out of range
# Exit code 3: Internal error
getchar() {
	if ! isunsigned "$1"; then
		printf "?"
		return 1
	fi
	if [ $1 -gt "$(printf "%s" "$2" | wc -c)" ]; then
		printf "?"
		return 2
	fi
        export CATMEOWSHELLUTILS_GETCHAR_OUTPUT="$(getstringpart "$1" "$2" 1)"
        let CATMEOWSHELLUTILS_GETCHAR_RET=$?
        if [ $CATMEOWSHELLUTILS_GETCHAR_RET -eq 1 ]; then
                printf "?"
                return 3
        fi
        if [ $CATMEOWSHELLUTILS_GETCHAR_RET -eq 2 ]; then
                printf "?"
                return ?
        fi
	printf "%s" "$CATMEOWSHELLUTILS_GETCHAR_OUTPUT"
	export CATMEOWSHELLUTILS_GETCHAR_RET= CATMEOWSHELLUTILS_GETCHAR_OUTPUT=
}

# Get the decimal equivalent of a character
# If there is an error, the result will be 0
# Exit code 1 - No first argument
# Exit code 2 - Wrong number characters, and a character index was not specified
# Exit code 3 - Character index (second argument) is not an integer 
getchardec() {
        if [ "$1" = "" ]; then
                printf "0"
                return 1
        fi
        if [ "$2" = "" ]; then
                if [ "$( printf "%s" "$1" | wc -l)" -eq 1 ]; then
                        env LC_TYPE=C printf "%d" "$1"
                        return 0
                else
                        printf "0"
                        return 2
                fi
        fi
        isinteger "$2"
        if [ $? -eq 1 ]; then
                printf "0"
                return 3
        else
                getchardec "$(getchar "$1" "$2")"
                return $?
        fi
}

# Check if the system has a specific program
checkforprogram() {
    type "$*" >/dev/null 2>/dev/null
    return $?
}

# Check if the system has all the specified programs
checkforprograms() {
    for i in "$@"; do
        checkforprogram "$i"
        if [ $? -eq 0 ]; then
            return 0
        fi
    done
    return 1
}

# Runs a command with a new shell executable specified by $SHELL
runinnewshellexecutable() {
    "$SHELL" -c "$*"
}

# Expands an arbitrary variable that may not be known until runtime.
expandvar() {
    runinnewshellexecutable "printf '%s' \"\${$1}\""
}

# Copies a string to a temporary file and sets the specified environment variable to the path of the temporary file
# The first argument is the variable name.
# All other arguments are strings to add to the file
copytotempfile() {
    CATMEOWSHELLUTILS_VARNAME="$1"
    shift
    export ${CATMEOWSHELLUTILS_VARNAME}="$(mktemp)"
    runinnewshellexecutable "echo \"$*\" > $(expandvar $CATMEOWSHELLUTILS_VARNAME)"
    export CATMEOWSHELLUTILS_VARNAME=
}

# Copies a string piped into this function to a temporary file and sets the specified enivronment variable to the path of the temporary file
# The first argument is the variable name
# All other arguments are ignored
# This is a wrapper ofer copytotempfile()
copytotempfile_stdin() {
    export CATMEOWSHELLUTILS_COPYTOTEMPFILE_STDIN="$(cat)"
    copytotempfile "$1" "$CATMEOWSHELLUTILS_COPYTOTEMPFILE_STDIN"
    export CATMEOWSHELLUTILS_COPYTOTEMPFILE_STDIN=
}

# Copies a string to a temporary file, makes it executable, and sets the specified environment variable to the path of the temporary file.
# The first argument is the variable name
# All other arguments are strings to add to the file.
# This is a wrapper over copytotempfile()
copytotempfile_executable() {
    copytotempfile "$@"
    chmod +x "$(expandvar "$1")"
}

# Copies a string via stdin to a temporary file, makes it executable, and sets the specified environment variable to the path of the temporary file.
# The first argument is the variable name.
# This is a wrapper over copytotempfile_stdin(), which is in turn a wrapper over copytotempfile()
copytotempfile_stdin_executable() {
    copytotempfile_stdin "$@"
    chmod +x "$(expandvar "$1")"
}

# Prints 1 if the input is 0, otherwise prints 0.
# Useful if you want to check an exit code that was copied to a variable or if you are checking $? and want 1 to be success and 0 to be failure (perhaps to set to a variable).
invert_exitcode() {
    if isinteger $1; then
        if [ $1 -eq 0 ]; then
            printf "1"
            return 0
        else
            printf "0"
            return 0
        fi
    fi
    return 1
}

# Evaluates a math expression using either bc, bash, or zsh, depending on what is installed.
# Fails if no possible options are installed. (exit code 1).
mathexpression() {
    if checkforprogram bc; then
        echo "$*" | bc -q
        return
    fi
    checkforprograms bash zsh
    export CATMEOWSHELLUTILS_BASHORZSH=$(invert_exitcode $?)
    checkforprograms bash zsh fish
    export CATMEOWSHELLUTILS_MATHEXPRESSION_USEGENERATEDSCRIPT=$(invert_exitcode $?)
    export CATMEOWSHELLUTILS_MATHEXPRESSION_BASHORZSHTEMPLATE="echo %s"
    export CATMEOWSHELLUTILS_MATHEXPRESSION_BASHORZSHTEMPLATE_ARG1="\$(($*))"
    if checkforprogram bash; then
        printf "#!/bin/bash\n$CATMEOWSHELLUTILS_MATHEXPRESSION_BASHORZSHTEMPLATE" "$CATMEOWSHELLUTILS_MATHEXPRESSION_BASHORZSHTEMPLATE_ARG1" | copytotempfile_stdin_executable "CATMEOWSHELLUTILS_MATHEXPRESSION_EXECUTABLE"
    fi
    if checkforprogram zsh; then
        printf "#!/bin/zsh\n$CATMEOWSHELLUTILS_MATHEXPRESSION_BASHORZSHTEMPLATE" "$CATMEOWSHELLUTILS_MATHEXPRESSION_BASHORZSHTEMPLATE_ARG1" | copytotempfile_stdin_executable "CATMEOWSHELLUTILS_MATHEXPRESSION_EXECUTABLE"
    fi
    if [ $CATMEOWSHELLUTILS_MATHEXPRESSION_USEGENERATEDSCRIPT -eq 1 ]; then
        "$CATMEOWSHELLUTILS_MATHEXPRESSION_EXECUTABLE"
	export CATMEOWSHELLUTILS_MATHEXPRESSION_EXITCODE=$?
        export CATMEOWSHELLUTILS_MATHEXPRESSION_EXECUTABLE=
        export CATMEOWSHELLUTILS_BASHORZSH=
        export CATMEOWSHELLUTILS_MATHEXPRESSION_BASHORZSHTEMPLATE=
        export CATMEOWSHELLUTILS_MATHEXPRESSION_BASHORZSHTEMPLATE_ARG1
        export CATMEOWSHELLUTILS_MATHEXPRESSION_USEGENERATEDSCRIPT=
        return $CATMEOWSHELLUTILS_EXITCODE
    fi
    return 1
}

crashonerror() {
	set -e
	if [ ! $? -eq 0 ]; then
		echo "ERROR: The crashonerror function is not supported in this shell (set -e failed)." >&2
		cleanup
		exit 1
	fi
	if [ "$SHELL" = "/bin/bash" ]; then
		trap 'export CATMEOWSHELLUTILS_BASH_LASTCOMMAND=$CATMEOWSHELLUTILS_BASH_CURRENTCOMMAND; export CATMEOWSHELLUTILS_BASH_CURRENTCOMMAND=$BASH_COMMAND' DEBUG
		trap 'echo "\"${CATMEOWSHELLUTILS_BASH_LASTCOMMAND}\" failed with exit code $?." >&2' EXIT
	else
		trap 'echo "A command failed with exit code $?." >&2' EXIT
	fi
}

# Cleans up any environment variables any utilities may have made and couldn't clean up due to technical or architectural limitations
cleanup() {
	export CATMEOWSHELLUTILS_MATHEXPRESSION_EXITCODE=
}
