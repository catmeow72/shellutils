#!/bin/env -Ssh -c 'echo This script should be run using a specific shell's command. >&2; exit 1'

# Source the shell utilities to test
. ShellUtils.sh

# Testing utilities
testpre() {
	echo "TEST: $*" >&2
}
testfail() {
	echo "TEST FAILED: $*" >&2
	exit 1
}
runwithret() {
	"$@"
	export RET=$?
}
testret() {
	export TESTS_INTENDED=$1
	export TESTS_ERRORMSG=$2
	shift
	shift
	runwithret "$@"
	if [ ! $RET -eq $RUNWITHRETTESTED_INTENDED ]; then
		testfail "$RUNWITHRETTESTED_ERRORMSG (returned $RET instead of 1)"
	fi
}
shouldpass() {
	export TESTS_ERRORMSG=$1
	shift
	runwithret "$@"
	if [ ! $RET -eq 0 ]; then
		testfail "$TESTS_ERRORMSG (should pass; returned $RET)"
	fi
}
shouldfail() {
	export TESTS_ERRORMSG=$1
	shift
	runwithret "$@"
	if [ $RET -eq 0 ]; then
		testfail "$TESTS_ERRORMSG (should fail; returned $RET)"
	fi
}

# isinteger(string) tests
testpre "isinteger"
shouldfail "Non-integer string" isinteger "Not an integer"
shouldfail "Empty string" isinteger ""
shouldpass "Actual integer" isinteger 1

# getstringpart(int, int, string) tests
testpre "getstringpart"
testret 1 "First argument wrong" getstringpart "Not an integer" 1 "TESTSTRING"
testret 1 "Second argument wrong" getstringpart 1 "Not an integer" "TESTSTRING"
testret 2 "Arguments out of range" getstringpart 99 99 "TESTSTRING"
